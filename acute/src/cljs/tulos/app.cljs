(ns tulos.app
  (:require [clojure.browser.repl]
            [mrhyde.core :as mrhyde]))

(def angular (this-as ct (aget ct "angular")))

#_(defn ^:export bootstrap []
    (mrhyde/bootstrap))

(mrhyde/bootstrap)

#_(defn PhoneListCtrl [$scope, $http]
    (-> $http (.get "phones/phones.edn")
            (.success 
                    (fn [data]
                              (aset $scope "phones" data))))
    (aset $scope "orderProp" "age"))

(defn hello
  []
  (js/alert "hello !!!"))

(defn whoami
  []
  (.-userAgent js/navigator))

(defn FirstCtrl [$scope]
  (aset $scope "message" "Salve"))
