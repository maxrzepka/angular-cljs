# Use Angularjs with Clojure(script)

## Approaches

Investigate various approach to write angularjs application in clojurescript 

   - [Teach cljs about js](http://keminglabs.com/blog/angular-cljs-weather-app/) by @lynaghk along with [source code](https://github.com/lynaghk/todoFRP/tree/master/todo/angular-cljs).
   - Teach js about cljs : 
      - [acute](https://github.com/dribnet/acute) this approach is explained by @dribnet : it's called [Metaprogramming Polyfill](http://www.infoq.com/presentations/javascript-clojurescript).
      - [clang](https://github.com/pangloss/clang)

Other interesting source :

   - Great angular-js tutorials : [egghead.io](http://egghead.io).
   - [todoFRP](https://github.com/lynaghk/todoFRP/) various TODO-apps made in clojurescript.
   - [js cljs synonyms](http://himera.herokuapp.com/synonym.html).

## Dev Set up


### Running REPLs

Steps to launch cljs REPL inside the project with the leinigen plugin [austin](https://github.com/cemerick/austin/tree/master/browser-connected-repl-sample)

1.   `lein do cljsbuild clean, cljsbuild once , repl`

2.   `(run)` 
     launch web server containing page with js script will connect at load to a REPL :
     `[:script (cemerick.austin.repls/browser-connected-repl-js)]`

3.   In clojure REPL create var `repl-env` starting a browser REPL
     `(def repl-env (reset! cemerick.austin.repls/browser-repl-env (cemerick.austin/repl-env)))`
     Browser-REPL ready @ http://localhost:39135/6879/repl/start

4.   Load page containing the connecting js script here http://localhost:8080

5.   Launch cljs REPL with repl-env 
     `(cemerick.austin.repls/cljs-repl repl-env)`

6.   `cljs.user=> (-> js/document .-body (.setAttribute "style" "background:red"))`
     The command above will change the background color to red.


### Emacs Integration

   - Use clojure mode for editing clojurescript files
   - Having 2 REPLs (clj, cljs) `C-c C-k` compilation shorcut will always use the active REPL. To swap between them use `C-c M-r` 
   - More details on [nrepl.el documentation](https://github.com/clojure-emacs/nrepl.el#managing-multiple-sessions).

### Dev Workflow

1.   Launch 2 REPLs(clj, cljs) on the same server and switch between the two.
2.   to valid a change in HTML angularjs page : first re-load `web.clj` then re-load web page.
