(defproject tulos/plain-angular "TESTING"
  :source-paths ["src/clj" "src/cljs"]
  :dependencies [[org.clojure/clojure "1.5.1"]
                  [org.clojure/clojurescript "0.0-1859"]
                 ;[org.clojure/clojurescript "0.0-1878"]
                 [ring "1.2.0"]
                 [compojure "1.1.5"]
                 [enlive "1.1.1"]]
  :profiles {:dev {:repl-options {:init-ns tulos.web}
                   :plugins [[com.cemerick/austin "0.1.0"]
                             [lein-cljsbuild "0.3.3"]]
                   :cljsbuild {:builds [{:source-paths ["src/cljs"]
                                         :compiler {:output-to "resources/public/app.js"
                                                    :optimizations :simple
                                                    :pretty-print true}}]}}})

