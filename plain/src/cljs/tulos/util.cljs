(ns tulos.util
  (:require [goog.object :as gobject]))

;;Make JavaScript objects and arrays place nicely with ClojureScript by implementing lookup protocols and acting like Clojure's transient collections

(defn strkey [x]
  (if (keyword? x)
    (name x)
    x))

(extend-type object
  ILookup
  (-lookup
    ([o k]
       (aget o (strkey k)))
    ([o k not-found]
      (let [s (strkey k)]
        (if (goog.object.containsKey o s)
          (aget o s) 
          not-found))))

  IEmptyableCollection
  (-empty [_]
    (js-obj))

  ITransientCollection
  (-conj! [o [k v]]
    (assoc! o k v))
  (-persistent! [_]
    (throw (js/Error. "JavaScript object isn't a real transient, don't try to make it persistent.")))

  ITransientAssociative
  (-assoc! [o k v]
    (aset o (strkey k) v)
    o)

  ITransientMap
  (-dissoc! [o key]
    (gobject/remove o key)
    o))

(extend-type array
  IEmptyableCollection
  (-empty [a]
    (array))

  ITransientCollection
  (-conj! [a x]
    (.push a x)
    a)
  (-persistent! [_]
    (throw (js/Error. "JavaScript array isn't a real transient, don't try to make it persistent.")))

  ITransientAssociative
  (-assoc! [a k v]
    (aset a (strkey k) v)
    a))
