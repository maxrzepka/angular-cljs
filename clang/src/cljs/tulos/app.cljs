(ns tulos.app
  (:require [clojure.browser.repl]
            clang.js-types
            clang.directive.clangRepeat)
  (:require-macros [clang.angular :refer [def.controller]])
  (:use [clang.util :only [module]]))

(defn hello
  []
  (js/alert "hello !!!"))

(defn whoami
  []
  (.-userAgent js/navigator))

(def m (module "tulos.app"))

(def.controller m FirstCtrl [$scope]
  (assoc! $scope
          :message "Salve")
  (assoc! $scope :nums (range 1 10)))


(def.controller m ContractCtrl [$scope]
  (assoc! $scope
          :contracts [{:name "Corn Futures" :months [:h :k :n :u :z] :size 50000}
                      {:name "Soybean Futures" :months [:f :h :k :n :q :u :x] :size 5000}]))
